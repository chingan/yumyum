<?php

require_once("controller/admin.php");

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>YumYumm Admin</title>
		<meta name="description" content="">

		<link href='http://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

		<link rel="icon" type="image/png" href="favicon.png" sizes="32x32">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" href="css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="css/dataTables.tableTools.css">
		<link rel="stylesheet" href="css/custom.css">

		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="js/lib/modernizr.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/moment-with-locales.js"></script>
		<script src="js/bootstrap-datetimepicker.js"></script>
		<script src="js/jquery.dataTables.min.js"></script>
		<script src="js/dataTables.tableTools.min.js"></script>
	</head>

	<body>

		<?php require_once("header.php"); ?>

       	<div class="container text-center">
		    <div class="row">
		    	<div class='col-sm-2 padding-top spacer-top'>
		    		<span>View Orders For:</span>
		    	</div>

		        <div class='col-sm-2 spacer-top'>
		            <input type='text' class="form-control text-center" id='datetimepicker' />
		        </div>

		        <div class='col-sm-1 spacer-top'>
		            <button type="button" class="btn btn-default btn-theme btn-date">Go!</button>
		        </div>
		    </div>
		</div>

		<div class="spacer-top well admin-container container">
			<h1 class="text-center">Orders for <?php echo isset($_GET['date'])? date("d M Y", strtotime($_GET['date'])) : date("d M Y"); ; ?></h1>
			<table class="table-hover" id="orders-table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
						<th>Phone Number</th>
						<th>Address</th>
						<th>Email</th>
						<th>Total Price</th>
					</tr>
				</thead>
		 
				<tbody>
					<?php 
						$currentOrderId = -1; 
						$childRowData = array();
					?>
					<?php if($orders): ?>
						<?php foreach($orders as $order): ?>
							<tr data-child-value=<?php echo http_build_query($order['orders_items'], '', ';'); ?> >
								<td class="details-control"></td>
								<td><?php echo $order['name'] ?></td>
								<td><?php echo $order['phone_number'] ?></td>
								<td><?php echo $order['address'] ?></td>
								<td><?php echo $order['email_address'] ?></td>
								<td><?php echo $order['total_price'] ?></td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</body>

	<script>

		function format(value) {
			var itemsArray = value.split(";"); 
			html = "<div class='order-items-container'><b>Total Order Items</b>";
			for (var i = 0; i < itemsArray.length; i++) {
				var infoArray = itemsArray[i].split("=");
				html += "<div>"+infoArray[0].replace(/\+/g, " ")+": "+infoArray[1]+"</div>";
			}
			html += "</div>";
			return html;
		}

		$(document).ready(function(){

			$("#datetimepicker").datetimepicker({
				format: 'YYYY-MM-DD'
			});

			var table = $('#orders-table').DataTable({
				"dom": 'T<"clear">lfrtip',
				"tableTools": {
					 "sSwfPath": "swf/copy_csv_xls_pdf.swf"
				}
			});

			$('#orders-table').on('click', 'td.details-control', function () {
				var tr = $(this).closest('tr');
				var row = table.row( tr );
		 
				if ( row.child.isShown() ) {
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}
				else {
					// Open this row
					row.child(format(tr.data('child-value'))).show();
					tr.addClass('shown');
				}
			});

			// Auto expand all child rows
			$("td.details-control").trigger("click");

			$(".btn-date").click(function() {
				var selectedDate = $("#datetimepicker").val();
				if (selectedDate != "") {
					window.location.href = "admin.php?date=" + selectedDate;
				}
			});
		});
	</script>

