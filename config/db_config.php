<?php
/**
 * Database config variables
 */
if (file_exists("config/db_config_local.php")) {
	require_once("config/db_config_local.php");
} else {
	define("DB_HOST", "localhost");
	define("DB_DATABASE", "yumyumm");
	define("DB_USER", "root");
	define("DB_PASSWORD", "");
}
?>