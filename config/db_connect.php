<?php
 
class DBConnect {
 
    // Constructor
    function __construct() {
 
    }
 
    // Destructor
    function __destruct() {
        // $this->close();
    }
 
    // Connecting to database
    public function connect() {
        require_once 'db_config.php';

        $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
        mysql_query("SET CHARACTER SET utf8", $con); 
        mysql_query("SET NAMES 'utf8'", $con); 

        // Selecting database
        mysql_select_db(DB_DATABASE);
 
        // Return database handler
        return $con;
    }
 
    // Closing database connection
    public function close() {
        mysql_close();
    }
 
}
 
?>