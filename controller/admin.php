<?php
session_start();
if (!isset($_SESSION["User.Id"])) {
	header('location:login.php');
}

require_once('controller/app.php');
$app = new AppController();

$date = isset($_GET['date'])? $_GET['date'] : null;
$orders = $app->getAllOrders($date);

?>