<?php

class AppController {

	function __construct() {
		require_once('config/db_connect.php');
		$this->db = new DBConnect();
		$this->db->connect();
	}

	/**
	 * Sanitize sql string to prevent sql injection
	 * @param string
	 * returns sanitized string
	 */
	public function sqlSanitize($string) {
		return mysql_real_escape_string(trim($string));
	}

	/**
	 * Decrypting password
	 * @param password
	 * returns hashed string
	 */
	public function checkhashSSHA($password) {
		$hash = base64_encode(sha1($password, true));
		return $hash;
	}

	public function login($username, $password) {

		$username = $this->sqlSanitize($username);
		$password = $this->sqlSanitize($password);

		$result = mysql_query("SELECT * FROM users WHERE username = '$username' and status = 1") or die(mysql_error());
		$rowCount = mysql_num_rows($result);
		if ($rowCount > 0) {
			$result = mysql_fetch_array($result);

			$encryptedPassword = $result['password'];
			$hashedPassword = $this->checkhashSSHA($password);

			// Check for password equality
			if ($encryptedPassword == $hashedPassword) {
				// If user authentication success
				session_start();
				$_SESSION["User.Id"] = $result['id'];
                return true;
			}
		}
		return false;
	}

	public function getAllOrders($date = null) {

		$query = "SELECT id, name, phone_number, address, email_address, total_price
			FROM orders o ";

		if ($date) {
			$query .= "WHERE created_at = '$date' ";
		}

		$query .= "ORDER BY id";

		$result = mysql_query($query) or die(mysql_error());

		$rowCount = mysql_num_rows($result);
		if ($rowCount > 0) {
			$orders = array();
			while ($row = mysql_fetch_array($result)) {
				$order = array();
				$order['id'] = $row['id'];
				$order['name'] = $row['name'];
				$order['phone_number'] = $row['phone_number'];
				$order['address'] = $row['address'];
				$order['email_address'] = $row['email_address'];
				$order['total_price'] = $row['total_price'];

				/* Retrieve the order items */
				$currentOrderId =  $row['id'];
				$itemsResult = mysql_query("SELECT name, COUNT(oi.item_id) as quantity 
					FROM orders_items oi 
					JOIN items i on (oi.item_id = i.id)
					WHERE oi.order_id = $currentOrderId
					GROUP BY oi.item_id") or die(mysql_error());
				$items = array();
				while ($row = mysql_fetch_array($itemsResult)) { 
					$items[$row['name']] = $row['quantity'];
				}
				$order['orders_items'] = $items;

				array_push($orders, $order);
			}
			return $orders;
		}
		return false;
	}

	public function getAllItems() {
		$result = mysql_query("SELECT *
			FROM items i 
			WHERE status = 1
			ORDER BY id") or die(mysql_error());

		$rowCount = mysql_num_rows($result);
		if ($rowCount > 0) {
			$items = array();
			while ($row = mysql_fetch_array($result)) {
				$item = array();
				$item['id'] = $row['id'];
				$item['name'] = $row['name'];
				$item['description'] = $row['description'];
				$item['price'] = $row['price'];
				$item['image_path'] = $row['image_path'];

				array_push($items, $item);
			}
		}
		return $items;
	}

	public function removeItem($itemId) {
		$result = mysql_query("UPDATE items SET status = 0 where id = $itemId") or die(mysql_error());

		if ($result && mysql_affected_rows()) {
			return true;
		}
		
		return false;
	}

	public function addItem($itemName, $itemDesc, $itemPrice, $itemPath) {
		$result = mysql_query("INSERT INTO items (name, description, price, image_path) VALUES ('$itemName', '$itemDesc', '$itemPrice', '$itemPath')") or die(mysql_error());

		if ($result && mysql_affected_rows()) {
			return true;
		}
		
		return false;
	}

	public function addOrder($orderName, $orderPhone, $orderAddress, $orderEmail) {
		$result = mysql_query("INSERT INTO orders (name, phone_number, address, email_address, created_at) VALUES ('$orderName', '$orderPhone', '$orderAddress', '$orderEmail', NOW())") or die(mysql_error());

		if ($result && mysql_affected_rows()) {
			return mysql_insert_id();
		}
		
		return false;
	}

	public function addOrdersItems($orderId, $itemId) {
		$result = mysql_query("INSERT INTO orders_items (order_id, item_id) VALUES ('$orderId', '$itemId')") or die(mysql_error());

		if ($result && mysql_affected_rows()) {
			return true;
		}
		
		return false;
	}

	public function completeOrder($orderId) {
		$quantityResult = mysql_query("UPDATE orders SET quantity = (SELECT COUNT(*) FROM orders_items WHERE order_id = '$orderId') WHERE id = '$orderId'") or die(mysql_error());
		$priceResult = mysql_query("UPDATE orders SET total_price = (SELECT SUM(price) FROM orders_items JOIN items on (orders_items.item_id = items.id) WHERE order_id = '$orderId') WHERE id = '$orderId'") or die(mysql_error());

		if ($quantityResult && $priceResult) {
			return true;
		}
		
		return false;
	}

	public function getNextIndex($tableName) {
		$result = mysql_query("SELECT AUTO_INCREMENT as next_index FROM information_schema.tables WHERE table_name = '$tableName'") or die (mysql_error());
		$row = mysql_fetch_array($result);
		return $row['next_index'];
	}
}

?>