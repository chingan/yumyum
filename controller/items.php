<?php

session_start();
if (!isset($_SESSION["User.Id"])) {
	header('location:login.php');
}

require_once('controller/app.php');
$app = new AppController();
$items = $app->getAllItems();

?>