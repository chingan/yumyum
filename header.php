<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<nav class="admin-header navbar navbar-default" role="navigation">
	<div class="col-md-12"><img class="logo-img" src="img/logo.png" /></div>
	<a href="admin.php"><div class="col-md-4 admin-orders spacer-top spacer-bottom <?php if (basename($_SERVER['PHP_SELF']) == "admin.php") echo 'active'; ?>"><i class="fa fa-clipboard"></i> ORDERS</div></a>
	<a href="items.php"><div class="col-md-4 admin-menus spacer-top spacer-bottom <?php if (basename($_SERVER['PHP_SELF']) == "items.php") echo 'active'; ?>"><i class="fa fa-cutlery"></i> MENUS</div></a>
	<a href="logout.php"><div class="col-md-4 admin-logout spacer-top spacer-bottom"><i class="fa fa-sign-out"></i> LOGOUT</div></a>
</nav>