<?php

$itemRemoved = false;
$itemAdded = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	require_once('controller/app.php');
	$app = new AppController();

	if (isset($_POST['itemId'])) {
		if ($app->removeItem($_POST['itemId'])) {
			$itemRemoved = true;
		}
	} else if (isset($_POST['item-name'])){
		// Get the next index to prepend into file name to avoid naming conflict
		$nextIndex = $app->getNextIndex("items");

		// Save the file into the filesystem
		$name = $_FILES["new-item-image"]["name"];
		$tmp_name = $_FILES['new-item-image']['tmp_name'];
		$error = $_FILES['new-item-image']['error'];

		$location = 'img/items/';
		$fileName = $location . $nextIndex . "-" . $name;
   		move_uploaded_file($tmp_name, $fileName);

   		// Add the entry into the DB
		if ($app->addItem($_POST['item-name'], $_POST['item-desc'], $_POST['item-price'], $fileName)) {
			$itemAdded = true;
		}
	}
	
}

require_once("controller/items.php");

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>YumYumm Admin</title>
		<meta name="description" content="">

		<link href='http://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

		<link rel="icon" type="image/png" href="favicon.png" sizes="32x32">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="css/dataTables.tableTools.css">
		<link rel="stylesheet" href="css/custom.css">

		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.dataTables.min.js"></script>
		<script src="js/dataTables.tableTools.min.js"></script>
	</head>

	<body>

		<?php require_once("header.php"); ?>

		<div class="well admin-container container">
			<h1 class="text-center">Menu Items</h1>
			<table class="table-hover" id="items-table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Image</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th></th>
					</tr>
				</thead>
		 
				<tbody>
					<?php foreach($items as $item): ?>
						<tr ref="<?php echo $item['id']; ?>">
							<td><img class="item-image" src="<?php echo $item['image_path']; ?>" alt=""></td>
							<td><?php echo $item['name']; ?></td>
							<td><?php echo $item['description']; ?></td>
							<td><?php echo $item['price']; ?></td>
							<td>
								<form id="item-removal-form" method="post">
									<button type="button" class="btn btn-default btn-remove"> Remove </button>
								</form>
							</td>
						</tr>
					<?php endforeach; ?>
					<div class="alert alert-success item_removed spacer-top" role="alert">You have successfully removed your menu item.</div>
					<div class="alert alert-success item_added spacer-top" role="alert">You have successfully added your menu item.</div>
				</tbody>
			</table>

			<button type="button" class="btn btn-default btn-theme btn-add spacer-top">Add New Menu Items</button>
		</div>

		<div class="modal fade" id="new-item-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Add New Menu Items</h4>
					</div>

					<div class="modal-body">

						<form id="add-new-item-form" action="" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="item-name" class="control-label">Name:</label>
								<input type="text" class="form-control" name="item-name" id="item-name">
							</div>

							<div class="form-group">
								<label for="item-desc" class="control-label">Description:</label>
								<textarea class="form-control" name="item-desc" id="item-desc"></textarea>
							</div>

							<div class="form-group">
								<label for="item-price" class="control-label">Price:</label>
								<input type="text" class="form-control" name="item-price" id="item-price">
							</div>

							<input type="file" name="new-item-image" id="new-item-image">

							<img id="image-preview" class="spacer-top" src="#" alt="" />
						</form>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary btn-theme btn-add-item">Add</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</body>

	<script>

		$(document).ready(function(){
			var table = $('#items-table').DataTable({
				"dom": 'T<"clear">lfrtip',
				"tableTools": {
					 "sSwfPath": "swf/copy_csv_xls_pdf.swf"
				}
			});

			$(document).on("click",".btn-remove",function() {
				var itemID = $(this).parent().parent().parent().attr("ref");

				if (!confirm("Are you sure you want to remove this menu item? Please note that this action is irreversible.")) {
					return false;
				}

				var input = $("<input>").attr("type", "hidden").attr("name", "itemId").val(itemID);
				$('#item-removal-form').append($(input));
				$("#item-removal-form").submit();
			});

			/* Display success message if the item is removed */
			<?php if ($itemRemoved): ?>
				$(".item_removed").fadeIn('slow');
				setTimeout(function(){ $(".item_removed").fadeOut('slow'); }, 2500);
			<?php endif; ?>

			<?php if ($itemAdded): ?>
				$(".item_added").fadeIn('slow');
				setTimeout(function(){ $(".item_added").fadeOut('slow'); }, 2500);
			<?php endif; ?>

			$(".btn-add").click(function() {
				$("#new-item-modal").modal();
			});

			$(".btn-add-item").click(function() {

				var price = $("#item-price").val();

				if(!isNumber(price)) {
					alert("Please enter number only in the Price field.");
					return false;
				}

				$("#add-new-item-form").submit();
			});

			$("#new-item-image").change(function(){
				$("#image-preview").show();
				readURL(this);
			});
			
		});

		function readURL(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#image-preview').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		function isNumber(n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
		}
	</script>

