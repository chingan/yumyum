<?php 

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	require_once('controller/app.php');
	$app = new AppController();

	$username = $_POST['username'];
	$password = $_POST['password'];

	if ($app->login($username, $password)) {
		header("location:admin.php");
		exit();
	}

	header("location:login.php?err=1");
}

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>YumYumm Login</title>
		<meta name="description" content="">

		<link href='http://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,700&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

		<link rel="icon" type="image/png" href="favicon.png" sizes="32x32">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.min.css">
		<link rel="stylesheet" href="css/custom.css">

		<script src="js/lib/modernizr.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>

	<body>

		<div class="well center-block login-form-container">

			<form method="POST" id="login-form">
				<h3 class="text-center">YumYumm Login</h3>

				<?php
					if (isset($_GET["err"])) {
						echo "<div class='bg-warning text-center'>Username or password incorrect</div>";
					}
				?>

				<input type="text" class="form-control spacer-top" name="username" required autofocus placeholder="Username"/>

				<input type="password" class="form-control spacer-top" name="password" required placeholder="Password"/>

				<button type="submit" class="btn btn-default btn-theme login-btn spacer-top">Login</button>

			</form>

		</div>

	</body>