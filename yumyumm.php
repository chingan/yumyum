<?php

require_once("controller/index.php");

?>
<li class="product-page-item js-product-page" data-category="burger">
	<div class="prod-page-view">
		<ul class="prod-page-list">
			<li class="cat-products-list prod-page full-width">
				<div class="order-block">
					<p class="added-products l-dis-ib">Added products <span class="products-number"><span class="js-products-counter"></span></span></p>
					<span class="order-now-btn js-order-btn l-dis-ib">Order Now</span>
				</div>
				<div class="products-list-block">
					<ul class="products-list js-products-list">
						<?php foreach ($items as $item): ?>
							<li class="products-item js-product-item">
								<div class="product-view l-dis-ib">
									<a href="<?php echo $item['image_path']; ?>" class="js-imagelightbox">
										<span class="hover-view" data-icon="&#xe08b;"></span>
										<img src="<?php echo $item['image_path']; ?>" alt="<?php echo $item['description']; ?>">
									</a>
								</div>
								<div class="product-short-info l-dis-ib">
									<p class="prod-name js-prod-title"><?php echo $item['name'] . "<br>" . $item['description']; ?></p>
									<p class="prod-code js-prod-code"><?php echo $item['id']; ?></p>
								</div>
								<div class="prod-price-block l-dis-ib">
									<span class="add-prod-btn js-add-to-card product-control-btn l-dis-ib">+</span>
									<p class="prod-price js-prod-price l-dis-ib"><?php echo $item['price']; ?><span class="currency"></span></p>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</li>